"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Maintainer:
"       Conor Wade
"       http://conorwade.com - conorwade@gmail.com
"
" Version:
"       0.3
"
" Sections:
"    -> Plug
"    -> General
"    -> UI
"    -> Editing
"    -> Git
"    -> Search
"    -> Plugin Specific
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plug
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Load plugins through Plug
call plug#begin('~/.config/nvim/plugged')

Plug 'morhetz/gruvbox'
Plug 'mhinz/vim-startify'
Plug 'itchyny/lightline.vim'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'tomtom/tcomment_vim'
Plug 'alvan/vim-closetag'
Plug 'Numkil/ag.nvim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'mxw/vim-jsx', { 'for': 'javascript' }
Plug 'elmcast/elm-vim', { 'for': 'elm' }
Plug 'gorodinskiy/vim-coloresque'
Plug 'ryanss/vim-hackernews'
Plug 'Shougo/deoplete.nvim'
Plug 'Shougo/neco-vim'
Plug 'Shougo/neoinclude.vim'
Plug 'Raimondi/delimitMate'
" Plug 'tpope/vim-fireplace', { 'for': 'clojure' }

" Add plugins to &runtimepath
call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Use Vim settings, rather then Vi settings
set nocompatible

" Sets how many lines of history VIM has to remember
set history=700

" " Enable filetype plugins
filetype plugin indent on

" " Set to auto read when a file is changed from the outside
set autoread

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = "\<Space>"
let g:mapleader = "\<Space>"

" No errors sounds or blinking
set vb t_vb=
" Set Timeout
set tm=500

" Remember info about open buffers on close
set viminfo^=%

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => UI
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Enable syntax highlighting
syntax enable

let g:lightline = { 'colorscheme': 'gruvbox' }

" Set colorscheme
let g:gruvbox_contrast_dark='soft'
set background=dark
colorscheme gruvbox

" Disabled Polyglot language packages
let g:polyglot_disabled = ['elm']

" Show line numbers
set number
set ruler
set numberwidth=4
set relativenumber number

" Always show the status line
set laststatus=2

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" Height of the command bar
set cmdheight=1

" Highlight the current line
set cursorline

" Hide scrollbars
set guioptions=

" Disable highlight when <leader><cr> is pressed
map <silent> <leader><cr> :noh<cr>

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Weird hack for mapping <Alt+l> and <Alt+j> to split right and down
map ¬ :vsp<cr>
map ∆ :sp<cr>

" More convienent keymappings
noremap H ^
noremap L g_
noremap K gg
noremap J G
" Make it easier to access command mode
nnoremap ; :

vmap < <gv
vmap > >gv

" Open new split panes to right and bottom, which feels more natural
set splitbelow
set splitright

" Specify the behavior when switching between buffers
" Switches to open buffer if file is already open
try
  set switchbuf=useopen
catch
endtry

" let $NVIM_TUI_ENABLE_TRUE_COLOR = 1 When iterm gets true color
let $NVIM_TUI_ENABLE_CURSOR_SHAPE = 1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Editing
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Configure self closing tags in html files
let g:closetag_filenames = '*.html,*.js'

" Treat <li> and <p> tags like the block tags they are
" TODO: See if this is needed
let g:html_indent_tags = 'li\|p'

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
" set whichwrap+=<,>,h,l

" Fix end of line on files that have none
" set fixeol

" Ignore files types
set wildignore=*.o,*~,*.pyc

" A buffer becomes hidden when it is abandoned
set hid

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch
" How many tenths of a second to blink when matching brackets
set mat=2


" Set utf8 as standard encoding and en_US as the standard language
" set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac

" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile

" This may not be needed on *nix machines
set nowritebackup

" Automatically :write before running commands
set autowrite

" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 2 spaces
set shiftwidth=2
set tabstop=2

" Linebreak on 500 characters
set lbr
set tw=80

set ai " Auto indent
set si " Smart indent
set wrap " Wrap lines

" Display extra whitespace
set list listchars=tab:»·,trail:·

" Delete trailing white space on save, useful for Python and CoffeeScript ;)
func! DeleteTrailingWS()
  exe "normal mz"
    %s/\s\+$//ge
  exe "normal `z"
endfunc
autocmd BufWrite * :call DeleteTrailingWS()

" Remember cursor position between sessions
autocmd BufReadPost *
  \ if line("'\"") > 0 && line ("'\"") <= line("$") |
  \   exe "normal! g'\"" |
  \ endif
" Center buffer around cursor when opening files
autocmd BufRead * normal zz

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Git
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Fugitive config
noremap <leader>gs :Gstatus<cr>
noremap <leader>gb :Gblame<cr>
noremap <leader>gc :Gcommit<cr>
noremap <leader>gd :Gdiff<cr>
noremap <leader>gp :Git push<cr>

" Always use vertical diffs
set diffopt+=vertical

" Git Gutter config
" Disable keybindings
let g:gitgutter_map_keys = 0
let g:gitgutter_sign_column_always = 1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Search
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" <Space + s> to search project
noremap <leader>s :Ag<SPACE>

if executable('ag')
  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor --ignore="*node_modules*" -g ""'

  " ag is fast enough that CtrlP doesn't need to cache
  let g:ctrlp_use_caching = 0
endif

let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'
let g:ctrlp_map = '<leader><leader>'
let g:ctrlp_cmd = 'CtrlP'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugin Specific
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Vim JSX
" Enable for .js files
let g:jsx_ext_required = 0

"Elm Vim
" Set up some keymappings
au FileType elm nmap <leader>b <Plug>(elm-make)
au FileType elm nmap <leader>m <Plug>(elm-make-main)
au FileType elm nmap <leader>t <Plug>(elm-test)
au FileType elm nmap <leader>r <Plug>(elm-repl)
au FileType elm nmap <leader>e <Plug>(elm-error-detail)
au FileType elm nmap <leader>d <Plug>(elm-show-docs)
au FileType elm nmap <leader>w <Plug>(elm-browse-docs)

" Deoplete (Auto-complete)
let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_smart_case = 1
inoremap <silent><expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
" inoremap <silent><expr> <cr> pumvisible() ? deoplete#mappings#manual_complete() : "\<cr>"

" Clojure
" associate *.boot with clojure filetype
au BufRead,BufNewFile *.boot setfiletype clojure

if has("nvim")
  " Open terminal and run lein figwheel
  nmap <Leader>term <C-w>v:terminal<CR>lein figwheel<CR><C-\><C-n><C-w>p
  " Evaluate anything from the visual mode in the next window
  vmap <buffer> <Leader>e y<C-w>wpi<CR><C-\><C-n><C-w>p
  " Evaluate outer most form
  nmap <buffer> <Leader>e ^v%,e
  " Evaluate buffer"
  nmap <buffer> <Leader>b ggVG,e
endif
